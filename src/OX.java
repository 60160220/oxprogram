import java.util.Scanner;

public class OX {
    static char board [][]= {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static int count = 0;
    static int count2 = 0;
    static int count3 =0;
    static char player1 = 'x';
    static char player2 = 'o';
    static int empty = 0;
    static int x,y;
    public static void main(String[] args) {
        printWelcome();
        while (true) {
            printBoard();
            printTurn();
            inputPlace();
            if (checkWin()==true){
                break;
            }
            if (checkDraw()==true){
                break;
            }
        }
        printBoard();
        printBye();
    }
    static void printWelcome(){
        System.out.println("Welcome to OX game.");
    }
    static void printBoard(){

        System.out.println("   1  2  3");
        for (int i = 0; i < 3; i++) {
            System.out.print(i + 1 + " ");
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == 'x') {
                    System.out.print(" x ");
                } else if (board[i][j] == 'o') {
                    System.out.print(" o ");
                } else {
                    System.out.print(" - ");
                }
            }
            System.out.println();
        }
    }
    static void printTurn(){

        if (count2 % 2 == 0) {

            System.out.println("X is turn");
        } else {

            System.out.println("O is turn");
        }
        count2++;
    }
    static void inputPlace(){
        System.out.println("Please input Row Col : ");
        Scanner kb = new Scanner(System.in);
        x=kb.nextInt();
        y=kb.nextInt();
        x=x-1;
        y=y-1;
        if (x < 0 || x > 2 || y < 0 || y > 2) {
            System.out.println("Over the limit");
            count2--;
            return;
        }
        if (board[x][y] == 'o'||board[x][y] == 'x') {
            System.out.println("Board position not empty");
            count2--;
            return;
        }
        if (count % 2 == 0) {
            board[x][y] = player1;

        } else {
            board[x][y] = player2;

        }
        count++;

    }
    static  boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if ((board[i][0] + board[i][1] + board[i][2]) == 'x' + 'x' + 'x') {
                System.out.println("X is Winner");
                return true;
            }
            if ((board[i][0] + board[i][1] + board[i][2]) == 'o' + 'o' + 'o') {
                System.out.println("O is Winner");
                return true;
            }
            if ((board[0][i] + board[1][i] + board[2][i]) == 'x' + 'x' + 'x') {
                System.out.println("X is Winner");
                return true;
            }
            if ((board[0][i] + board[1][i] + board[2][i]) == 'o' + 'o' + 'o') {
                System.out.println("O is Winner");
                return true;
            }
            if ((board[0][0] + board[1][1] + board[2][2]) == 'x' + 'x' + 'x'
                    || (board[0][2] + board[1][1] + board[2][0]) == 'x' + 'x' + 'x') {
                System.out.println("X is Winner");
                return true;
            }
            if ((board[0][0] + board[1][1] + board[2][2]) == 'o' + 'o' + 'o'
                    || (board[0][2] + board[1][1] + board[2][0]) == 'o' + 'o' + 'o') {
                System.out.println("O is Winner");
                return true;
            }
        }
        return false;

    }

    static boolean checkDraw(){
        if (count2==9){
            System.out.println("Game is draw");
            return true;

        }
        return false;
    }
    static void printBye(){
        System.out.println("Good bye");
    }
}