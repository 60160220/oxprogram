import java.util.Scanner;

public class Game {
    private Board board;
    private Player x;
    private Player o;

    public Game() {
        x = new Player('X');
        o = new Player('O');

    }

    public void Play() {
        while (true){
            playone();
        }
    }
    public void playone(){
        board = new Board(x,o);
        showWelcome();
        while (true) {
            showTable();
            showTurn();
            input();
            if (board.isFinish()){
                break;
            }
            board.switchPlayer();
        }
        showTable();
        showWinner();
        showStat();
    }

    private void showStat() {
        System.out.println(x.getName()+ " W,D,L:"
                + x.getWin()+","
                + x.getDraw()+","
                + x.getLose());
        System.out.println(o.getName()+ " W,D,L:"
                + o.getWin()+","
                + o.getDraw()+","
                + o.getLose());
    }
    private void showWinner() {
        Player player = board.getWinner();
        System.out.println(player.getName() + " win.......");
    }

    private void input() {
        Scanner kb = new Scanner(System.in);
        while (true){
            try {
                System.out.println("Please input Row Col : ");
                String input = kb.nextLine();
                String []str = input.split(" ");
                if (str.length!=2){
                    System.out.println(" Please input Row Col [1-3] (ex : 1 2)");
                    continue;
                }
                int row = Integer.parseInt(str[0])-1;
                int col = Integer.parseInt(str[1])-1;
                if (board.setTable(row,col) == false){
                    System.out.println("Board position not empty");
                    continue;
                }
                break;
            }catch (Exception e){
                System.out.println(" Please input Row Col [1-3] (ex : 1 2)");
                continue;
            }

        }



        //board.setTable(row,col);


    }

    private void showTurn() {
        Player player = board.getCurrentPlayer();
        System.out.println(player.getName()+" turn.");
    }

    private void showTable() {
        char[][] table = board.getTable();
        System.out.println("  1 2 3");
        for (int i = 0; i < table.length; i++) {
            System.out.print((i+1));
            for (int j = 0; j < table.length; j++) {
                System.out.print(" "+table[i][j]);
            }
            System.out.println();
        }

    }


    private void showWelcome() {
        System.out.println("Welcome to OX game.");

    }
}
